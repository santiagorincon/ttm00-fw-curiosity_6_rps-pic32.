#include "../Inc/led.h"
#include <stdint.h>
#include <plib.h>

/**
  * @brief  Turns LED on
  * @param  <LED> enummerated value of LED to turn on. See led.h
  */
void LED_on(uint8_t LED){
    switch (LED){
        case LED1:
            mPORTESetBits(BIT_4);
            break;
        case LED2:
            mPORTESetBits(BIT_6);
            break;
        case LED3:
            mPORTESetBits(BIT_7);
            break;
        default:
            break;
    }
}

/**
  * @brief  Turns LED off
  * @param  <LED> enummerated value of LED to turn off. See led.h
  */
void LED_off(uint8_t LED){
    switch (LED) {
        case LED1:
            mPORTEClearBits(BIT_4);
            break;
        case LED2:
            mPORTEClearBits(BIT_6);
            break;
        case LED3:
            mPORTEClearBits(BIT_7);
            break;
        default:
            break;
    }
}

/**
  * @brief  Toggle LED state
  * @param  <LED> enummerated value of LED to toggle. See led.h
  */
void LED_toggle(uint8_t LED){
    switch (LED){
        case LED1:
            mPORTEToggleBits(BIT_4);
            break;
        case LED2:
            mPORTEToggleBits(BIT_6);
            break;
     case LED3:
            mPORTEToggleBits(BIT_7);
            break;
        default:
            break;
    }
}

/**
  * @brief  Turns all LEDs off
  */
void LED_all_off(void){
    LED_off(LED1);
    LED_off(LED2);
    LED_off(LED3);
}

/**
  * @brief Sets GPIOs at boot. Must be called before any other function.
  */
void LED_setup(void){
    //LED GPIO setup 
    mPORTESetPinsDigitalOut(BIT_4|BIT_6|BIT_7); 
       
    LED_off(LED1);
    LED_off(LED2);
    LED_off(LED3);
    
}