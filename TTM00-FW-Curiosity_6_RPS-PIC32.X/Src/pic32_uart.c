#include "xc.h"
#include "plib.h"
#include "../Inc/pic32_uart.h"
#include "stdio.h"
#include <string.h>

unsigned char   U1TxBuf[U1TxBufSize];
unsigned char	U1RxBuf[U1RxBufSize];
unsigned int	U1RxHead=0;
unsigned int    U1RxTail=0;
unsigned int	U1RxCharCount=0;
unsigned char   U1Unread = 0 ;

unsigned char   U2TxBuf[U2TxBufSize];
unsigned char	U2RxBuf[U2RxBufSize];
unsigned int	U2RxHead=0;
unsigned int    U2RxTail = 0;
unsigned int	U2RxCharCount=0;
unsigned char   U2Unread = 0 ;

unsigned char   U3TxBuf[U3TxBufSize];
unsigned char	U3RxBuf[U3RxBufSize];
unsigned int	U3RxHead=0;
unsigned int    U3RxTail = 0;
unsigned int	U3RxCharCount=0;
unsigned char   U3Unread = 0 ;

// UART1 Functions
void UART1_init(unsigned long int UART_Baud){
    //PIC32MX470 uses PPS, so UART1 by default is NOT wired to RPF0 / RPF1
#if defined(__32MX470F512H__)
    CFGCONbits.IOLOCK = 0;
    
    U1RXRbits.U1RXR = 0b0100; // RPF1
    RPF0Rbits.RPF0R = 3;  
    
    CFGCONbits.IOLOCK = 1;
#endif
    
    UARTConfigure(UART1, UART_ENABLE_PINS_TX_RX_ONLY);
	UARTSetFifoMode(UART1, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY); //
	UARTSetLineControl(UART1, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
	UARTSetDataRate(UART1, PBCLK_FREQUENCY, UART_Baud);
	UARTEnable(UART1, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));

	// Configure UART RX Interrupt
	INTEnable(INT_SOURCE_UART_RX(UART1), INT_ENABLED);
	INTSetVectorPriority(INT_VECTOR_UART(UART1), INT_PRIORITY_LEVEL_4);
	INTSetVectorSubPriority(INT_VECTOR_UART(UART1), INT_SUB_PRIORITY_LEVEL_0);
}

unsigned char UART1_is_unread(void){
    return (U1Unread);
}

void UART1_clear_unread(void){
    U1Unread = 0;
}

uint8_t UART1_new_event(){
    if (UART1_is_unread()){
        UART1_clear_unread();
        if (!memcmp(U1RxBuf,"s",1))
            return 1;
        else
            return 0;
    }
}

void UART1_write_string(const char *string){
	while(*string != '\0')
	{
		while(!UARTTransmitterIsReady(UART1))	;
		UARTSendDataByte(UART1, *string);
		string++;
		while(!UARTTransmissionHasCompleted(UART1));
	}
}

// UART2 Functions
void UART2_init(unsigned long int UART_Baud){
    U2Unread = 0;
#if defined(__32MX470F512H__)
    CFGCONbits.IOLOCK = 0;  // Allows PPS
    
    U2RXRbits.U2RXR = 0b0101;   
    RPE5Rbits.RPE5R = 1;
    
    CFGCONbits.IOLOCK = 1;
#endif
    
    UARTConfigure(UART2, UART_ENABLE_PINS_TX_RX_ONLY);
	UARTSetFifoMode(UART2, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY); //
	UARTSetLineControl(UART2, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
	UARTSetDataRate(UART2, PBCLK_FREQUENCY, UART_Baud);
	UARTEnable(UART2, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));

	// Configure UART RX Interrupt
	INTEnable(INT_SOURCE_UART_RX(UART2), INT_ENABLED);
	INTSetVectorPriority(INT_VECTOR_UART(UART2), INT_PRIORITY_LEVEL_4);
	INTSetVectorSubPriority(INT_VECTOR_UART(UART2), INT_SUB_PRIORITY_LEVEL_1);
}

unsigned char UART2_is_unread(void){
    return (U2Unread);
}

void UART2_clear_unread(void){
    U2Unread = 0;
}

void UART2_write_string(const char *string){
	while(*string != '\0')
	{
		while(!UARTTransmitterIsReady(UART2))	;
		UARTSendDataByte(UART2, *string);
		string++;
		while(!UARTTransmissionHasCompleted(UART2));
	}
}

// UART3 Functions
void UART3_init(unsigned long int UART_Baud){
    U3Unread = 0;
#if defined(__32MX470F512H__)
    CFGCONbits.IOLOCK = 0;
    
    RPD2Rbits.RPD2R = 0001; 
    U3RXRbits.U3RXR = 0000;
    
    CFGCONbits.IOLOCK = 1;
#endif
    
    UARTConfigure(UART3, UART_ENABLE_PINS_TX_RX_ONLY);
	UARTSetFifoMode(UART3, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY); //
	UARTSetLineControl(UART3, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
	UARTSetDataRate(UART3, PBCLK_FREQUENCY, UART_Baud);
	UARTEnable(UART3, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));

	// Configure UART RX Interrupt
	INTEnable(INT_SOURCE_UART_RX(UART3), INT_ENABLED);
	INTSetVectorPriority(INT_VECTOR_UART(UART3), INT_PRIORITY_LEVEL_4);
	INTSetVectorSubPriority(INT_VECTOR_UART(UART3), INT_SUB_PRIORITY_LEVEL_2);
}

void UART3_write_string(const char *string){
	while(*string != '\0')
	{
		while(!UARTTransmitterIsReady(UART3))	;
		UARTSendDataByte(UART3, *string);
		string++;
		while(!UARTTransmissionHasCompleted(UART3));
	}
}

unsigned char UART3_is_unread(void){
    return (U3Unread);
}

void UART3_clear_unread(void){
    U3Unread = 0;
}

uint8_t UART2_and_UART3_new_event(){
    if (UART2_is_unread() && UART3_is_unread()){
        UART3_clear_unread();
        UART2_clear_unread();
        return 1;
    }
    return 0;
}

/******************************* UART Interrupts ************************/

void __ISR(_UART1_VECTOR, IPL4SOFT) IntUart1Handler(void){
   
	if(INTGetFlag(INT_SOURCE_UART_RX(UART1))){
       U1RxBuf[U1RxTail] = UARTGetDataByte(UART1);
		if(U1RxBuf[U1RxTail] == '\r')
       {   
           U1Unread = 1;
           U1RxTail = 0;
       }else{      
           U1RxTail++;
       }
       if(U1RxTail >= U1RxBufSize) U1RxTail = 0;
		INTClearFlag(INT_SOURCE_UART_RX(UART1));
	}
    
	if (INTGetFlag(INT_SOURCE_UART_TX(UART1)))
	{
		INTClearFlag(INT_SOURCE_UART_TX(UART1));
	}
}

void __ISR(_UART2_VECTOR, IPL4SOFT) IntUart2Handler(void){
    
	if(INTGetFlag(INT_SOURCE_UART_RX(UART2))){        
        U2RxBuf[U2RxTail] = UARTGetDataByte(UART2);
		if(U2RxBuf[U2RxTail] == '\r'){
            U2Unread = 1;
            U2RxTail = 0;
        }else
            U2RxTail++;
        if(U2RxTail >= U2RxBufSize) U2RxTail = 0;
		INTClearFlag(INT_SOURCE_UART_RX(UART2));
	}
	
	if (INTGetFlag(INT_SOURCE_UART_TX(UART2))){
		INTClearFlag(INT_SOURCE_UART_TX(UART2));
	}
}

void __ISR(_UART_3_VECTOR, IPL4SOFT) IntUart3Handler(void){
    
	if(INTGetFlag(INT_SOURCE_UART_RX(UART3))){     
        U3RxBuf[U3RxTail] = UARTGetDataByte(UART3);
		if(U3RxBuf[U3RxTail] == '\r'){ 
            U3Unread = 1;
            U3RxTail = 0;
        }else
            U3RxTail++;
        if(U3RxTail >= U3RxBufSize) U3RxTail = 0;
		INTClearFlag(INT_SOURCE_UART_RX(UART3));
	}
	
	if (INTGetFlag(INT_SOURCE_UART_TX(UART3))){
		INTClearFlag(INT_SOURCE_UART_TX(UART3));
	}
}