#ifndef LED_H
#define	LED_H

#include <stdint.h>

enum{
    LED_OFF,
    LED_ON
};

enum{
    LED1,
    LED2,
    LED3,
    LED_LAST
};

void LED_on(uint8_t LED);
void LED_off(uint8_t LED);
void LED_toggle(uint8_t LED);
void LED_all_off(void);
void LED_setup(void);

#endif /* LED_H */